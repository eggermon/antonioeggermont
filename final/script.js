/* 
 
Antonio Aranda Eggermont - Final Project Exercise

A very simple VR experience for Tic Tac Toe Game.

Summary:

I wanted to explore with WebVR technology and I explored A-Frame, which is a markup
language to create VR apps in HTML/JavaScript. Since I am very new to A-Frame and we
had limited time to work on this final project, I decided to build a minimalistic
Tic Tac Toe game. Eventually the game can be played between two people using 
Websockets to intercommunicate with a basic VR gear. The goal of this assignment was
to do a POC to explore WebVR markup language.

To play the game simply use  your mouse to point to the 3D squares and click to 
set the block. If you are using your phone, just simply point the cursor to one of 
the blocks with the VR gear on to set the block.

Two players can play the game using the same interface.
*/


const Game = function(){
  
  this.board = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
  ];

  this.playerOneScore = 0;
  this.playerTwoScore = 0;
  this.playerOneActive = true;
  this.loadScores();

  console.log('>>> finished initializing game');
};


Game.prototype.toString = function(){
    console.log("printing board");
    return this.board;
}

Game.prototype.loadScores = function(){

    let playerOneScoreLS = localStorage.getItem('playerOneScore');
    let playerTwoScoreLS = localStorage.getItem('playerTwoScore');
    let displayScores = false;

    console.log(">>>>>>>>>>>>>>");
    console.log(playerOneScoreLS);
    console.log(playerTwoScoreLS);
    console.log(">>>>>>>>>>>>>>");


    if (playerOneScoreLS !== null){
        console.log("loading score for player 1");
        this.playerOneScore = parseInt(playerOneScoreLS);
        displayScores = true;
    }
    
    if( playerTwoScoreLS !== null){
        console.log("loading score for player 2");
        this.playerTwoScore = parseInt(playerTwoScoreLS);
        displayScores = true;
    }

    console.log(">>>>>>>>>>>>>>>>>>>>>");
    console.log(this.playerOneScore);
    console.log(this.playerTwoScore);
    console.log(">>>>>>>>>>>>>>>>>>>>>");

    if (displayScores){
        document.getElementById('playerOneScore').setAttribute("text", `value: ${this.playerOneScore} ; color:black`);
        document.getElementById('playerTwoScore').setAttribute("text", `value: ${this.playerTwoScore} ; color:black`);
    }
}

Game.prototype.updateScore = function(){
    console.log("updating scores ...");

    localStorage.setItem("playerOneScore", parseInt(this.playerOneScore));
    localStorage.setItem("playerTwoScore", parseInt(this.playerTwoScore));

    document.getElementById('playerOneScore').setAttribute("text", `value: ${this.playerOneScore} ; color:black`);
    document.getElementById('playerTwoScore').setAttribute("text", `value: ${this.playerTwoScore} ; color:black`);
}

Game.prototype.resetBoard = function(){

    for( let i=0; i < 3; i++){
        for (let j = 0; j < 3; j++) {
            document.getElementById(`${i},${j}`).setAttribute('color','#C0C0C0');
        }
    }

    delete this.board
    this.board = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    document.getElementById('winner').setAttribute('text', 'value: Play again');

};


Game.prototype.winner = function(){

    let p1row = 0;
    let p2row = 0;
    let p1column = 0;
    let p2column = 0;

    for (let j = 0; j < 3; j++) {
        for (let i = 0; i < 3; i++) {
            if (this.board[j][i] === 1) {
                p1row += 1;
            }
            if (this.board[j][i] === -1) {
                p2row += 1;
            }
            if (this.board[i][j] === 1) {
                p1column += 1;
            }
            if (this.board[i][j] === -1) {
                p2column += 1;
            }
        }

        if (p1row === 3 || p1column===3 || 
            this.board[0][0] + this.board[1][1] + this.board[2][2]===3 || 
            this.board[0][2] + this.board[1][1] + this.board[2][0]===3) {
            
            document.getElementById('winner').setAttribute("text", "value: Player One wins!; color:black");
            
            //console.log(">>> about to update score player one");
            this.playerOneScore += 1;

            this.updateScore();
            setTimeout(this.resetBoard,6000);
        }
        if (p2row === 3 || p2column === 3 || 
            this.board[0][0] + this.board[1][1] + this.board[2][2] === -3 || 
            this.board[0][2] + this.board[1][1] + this.board[2][0] === -3) {

            document.getElementById('winner').setAttribute("text", "value: Player Two wins!; color:black");
            
            this.playerTwoScore += 1;
            this.updateScore();
            setTimeout(this.resetBoard,6000);            
        }
        
        else {
            p1row = 0;
            p2row = 0;
            p1column = 0;
            p2column = 0;
        }
    }
}

Game.prototype.clicked = function(boxPosition){
    console.log('clicking!');

    let boxArr = boxPosition.split(",");
    window.boxArr = boxArr;
    window.board = this.board;

    if (this.playerOneActive  && this.board[boxArr[0]][boxArr[1]] == 0) {
        document.getElementById(boxPosition).setAttribute('color', 'blue');
        this.board[boxArr[0]][boxArr[1]] = 1;
        this.playerOneActive  = false;
        this.winner();
    }

    else if (this.board[boxArr[0]][boxArr[1]] == 0) {
        document.getElementById(boxPosition).setAttribute('color', 'green');
        this.board[boxArr[0]][boxArr[1]] = -1;
        this.playerOneActive  = true;
        this.winner();
    }
}

const ticTacToe = new Game();
