/*

 ADDRESS LINE

 1.  Create variables for first Name,lastName, streetAddress, city, state, andzipCode.
     Use this information to create a formatted address block that could be printed onto an envelope.
 */

let firstName = "Antonio";
let lastName  = "Eggermont";
let streetAddress = "715 East Pine Street"
let city = "Seattle";
let state = "WA";
let zipCode = 98122;

console.log(`${firstName} ${lastName}\n ${streetAddress}\n ${city}, ${state} zip code: ${zipCode}`);

fullNameString = "Antonio Eggermont\n715 East Pine Street\nSeattle, WA 98122";

console.log(fullNameString);
fullNameTokens = fullNameString.split("\n");

/*
2. Youaregivenastringinthisformat: firstName lastName (assume no spaces in either) streetAddress
   city, state zip (could be spaces in city and state)
   Write code that is able to extract this full string into each variable. Hint: use indexOf, slice, and/or substring
 */

firstName = fullNameString.split("\n")[0].split(" ")[0];
lastName  = fullNameString.split("\n")[0].split(" ")[1];
streetAddress = fullNameString.split("\n")[1];
city = fullNameString.split("\n")[2].split(" ")[0].slice(0,-1);
state = fullNameString.split("\n")[2].split(" ")[1];
zipCode = fullNameString.split("\n")[2].split(" ")[2];

console.log( firstName );
console.log( lastName );
console.log(streetAddress);
console.log(city);
console.log(zipCode);





//fullName = fullNameTokens[0].split(" ");
//console.log(fullNameTokens[0].split(" "));

