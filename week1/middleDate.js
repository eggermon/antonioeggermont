
/*
    FIND THE MIDDLE DATE
    On your own own find the middle date (and time) between the following two dates:
    1/1/2019 00:00:00 and 4/1/2019 00:00:00
 */

date1 = new Date(2019, 0 , 1);
date2 = new Date(2019, 3 , 1);

date1.getTime()/1000
date2.getTime()/1000

let timeDifference = date2.getTime() - date1.getTime();
let halfTime = timeDifference / 2;
let middleDate = new Date(date1.getTime() + halfTime);

console.log(middleDate);
