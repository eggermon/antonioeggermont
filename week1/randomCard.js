/*
MATH PRACTICE:
    1. UsingtheMathobject,put together a code snippet that allows you to draw a random
       card with a value between 1 and 13 (assume ace is 1, jack is 11...).
    2. Draw 3 cards and use Math to determine the highest card.
    3. Pagliaccicharges$16.99fora13”pizzaand$19.99for a 17” pizza.
       What is the surface area for each of these pizzas?
    4. Whatisthecostpersquareinchofeachpizza?
*/

// Question 1 and 2:

const numOfCards = 3;
var arrayOfCards = new Array();

for ( let i = 0 ; i < numOfCards ; i++){
    arrayOfCards[i] = Math.floor(Math.random() * 11) + 1;
    console.log( `Card  ${arrayOfCards[i]}`);

}
console.log( `Highest Card: ${Math.max.apply(Math, arrayOfCards )}`);

// Question 3
const pizzaPrice1 = 16.99;
const pizza1Diameter= 13;
const pizzaPrice2 = 19.99;
const pizza2Diameter = 17;

let surfaceAreaPizza1 = Math.PI * Math.pow((pizza1Diameter/2), 2);
let surfaceAreaPizza2 = Math.PI * Math.pow((pizza2Diameter/2), 2);

console.log(`Surface Area of 13" pizza is ${surfaceAreaPizza1}`);
console.log(`Surface Area of 17" pizza is ${surfaceAreaPizza2}`);

// Question 4:
console.log(`Cost per square inch for 13" pizza is ${pizzaPrice1/surfaceAreaPizza1}`);
console.log(`Cost per square inch for 17" pizza is ${pizzaPrice2/surfaceAreaPizza2}`);



