/*
   Antonio Eggermont
   Object representation of myself
*/

let person = {
    firstName : 'Antonio',
    lastName : 'Aranda Eggermont',
    'favorite food' :'mediterranean',
    mom : {
        firstName : 'Marcela',
        lastName  : 'Eggermont',
        'favorite food' : 'french',
    },
    dad : {
        firstName : 'Antonio',
        lastName : 'Aranda',
        'favorite food' : 'mexican',
    },
};

console.log(person);
console.log(person['dad']['firstName']);
console.log(person['mom']['favorite food']);

console.log(personTest.dad.firstName);
console.log(personTest.mom['favorite food']);