

const assignmentDate = '1/21/2019';

// Convert to a Date instance
let dueDate = new Date(assignmentDate);

// Create dueDate which is 7 days after assignmentDate

dueDate.setDate(dueDate.getDate() + 7);

console.log(dueDate);
console.log()


// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log

dueDate.getFullYear();
(dueDate.getMonth() + 1).toString().padStart(2, '0');
dueDate.getDate().toString().padStart(2, '0');

htmlTag = `<time datetime="YYYY-MM-DD"> ${(dueDate.getMonth() + 1).toString().padStart(2, '0')} ${dueDate.getDate().toString().padStart(2, '0')}, ${dueDate.getFullYear()} </time>`;
console.log(htmlTag);

// Note: note sure if the datetime attribute in the time tag also needed to be replaced 
htmlTag2 = `<time datetime="${dueDate.getFullYear()}-${(dueDate.getMonth() + 1).toString().padStart(2, '0')}-${dueDate.getDate().toString().padStart(2, '0')}"> ${(dueDate.getMonth() + 1).toString().padStart(2, '0')} ${dueDate.getDate().toString().padStart(2, '0')}, ${dueDate.getFullYear()} </time>`;
console.log(htmlTag2);


