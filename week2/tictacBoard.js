/*
 * Antonio A Eggermont
 * Class 2 - Class Excercise - Tictac Board
 *  
*/

// Create an array to represent the tic-tac-toe board from class
let board = [['-','O','-'],['-','X','O'],['X','-','X']];

board.forEach( function(element){
    console.log(element.join(','));
});

// After the array is created, O claims the top right square. 
// Update this value

board[0][2] = 'O';

board.forEach( function(element){
    console.log(element.join(','));
});



