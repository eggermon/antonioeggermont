/*
 * Antonio A Eggermont
 * Class 2 - Excercise 2
 * 
 * 1. Create a function attack that accepts these 4 parameters: attackingPlayer, defendingPlayer, 
 *    baseDamage, and variableDamage.
 * 2. Create two objects player1 and player2. These objects should have two properties, 
 *    health (set to 10) and name (set to a string of your choosing)
 * 3. The function should do the following:
 *      1. Calculate total damage. This will equal baseDamage plus a
 *         random integer from 0 to the amount of the variableDamage
 *      2. Reduce the health property of the defendingPlayer by the amount of the calculated damage
 *      3. Return a string describing the attack, i.e. "Merlin hits James Bond for 4 damage"
 */


// Create player1 and player2 objects below
// Each should have a name property of your choosing, and health property equal to 10

const player1 = {
    health : 10,
    name : 'Antonio',
  };
  
  const player2 = {
    health : 10,
    name : 'Mark',
  };
  
  
  const attack = function(attackingPlayer, defendingPlayer, baseDamage, variableDamage){      
      let totalDamage = baseDamage + Math.floor(Math.random() * (variableDamage + 1 ));
      defendingPlayer.health -= totalDamage;
      return (` ${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage}` );
  }
  

  // DO NOT MODIFY THE CODE BELOW THIS LINE
  // Set attacker and defender.  Reverse roles each iteration
  let attackOrder = [player1, player2];
  
  // Everything related to preventInfiniteLoop would not generally be necessary, just adding to
  // safeguard students from accidentally creating an infinite loop & crashing browser
  let preventInfiniteLoop = 100;
  while (player1.health >= 1 && player2.health >= 1 && preventInfiniteLoop > 0) {
    const [attackingPlayer, defendingPlayer] = attackOrder;
    console.log(attack(attackingPlayer, defendingPlayer, 1, 2));
    attackOrder = attackOrder.reverse();
  
    preventInfiniteLoop--;
  }
  const eliminatedPlayer = player1.health <= 0 ? player1 : player2;
  console.log(`${eliminatedPlayer.name} has been eliminated!`);

  
                                                                