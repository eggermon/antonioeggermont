/*
 * Antonio A Eggermont
 * Class 2 - Excercise 1
 * 
 * You are given a string, a user input for their email, make sure it is in correct 
 * email format.  Should be 1 or more characters, then @ sign, then 1 or more characters, 
 * then dot, then one or more characters -no whitespace:foo@bar.baz
 * 
 */

console.log(" >>> Validating Email Addresses <<<");

const myEmail = 'foo@bar.baz'; // good email
const badEmail = 'badmail@gmail'; // bad email
let pattern = /^\w+@\w+\.\w+/;


(pattern.exec(myEmail)) ? console.log("Valid email address") : console.log("Invalid Email Address");
(pattern.exec(badEmail)) ? console.log("Valid email address") : console.log("Invalid Email Address");


