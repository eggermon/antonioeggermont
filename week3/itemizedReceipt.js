/*
 * Antonio A Eggermont
 * Class 2 - Excercise 3 - ITEMIZED RECEIPT
 * 
 * Change logReceipt so that the first parameter is a taxRate which takes a 
 * number (i.e. 0.1 for 10% tax). Any additional arguments will be objects 
 * with two properties, descr and price (same as above).
 * 
 * Before logging the total,log a Subtotal and Tax.
 */


 const logReceipt = function( taxRate , ...  items){
    
    let subTotal = 0;
    let totalTax = 0;
    let grandTotal = 0;
    
    for ( item of items){
        subTotal += item.price;
        totalTax += item.price * taxRate;
    }
    
    grandTotal = subTotal + totalTax;

    console.log(`> Subtotal: ${subTotal}`);
    console.log(`> Total Tax (${taxRate * 100}%): ${totalTax}`);
    console.log(`> Grand Total: ${grandTotal.toFixed(2)}`);


 }


const bud = {descr: 'Bud Light', price: 3.99};
const burger = {descr: 'Hamburger', price: 6.99};


logReceipt(0.1, bud, burger);
