/*
 * Antonio A Eggermont
 * Class 3 - Homework Excercise 2 - Soccer Standings 
 */

 (function(){

    console.log('>>> Soccer Standings  <<<');

    const RESULT_VALUES = {
        w: 3,
        d: 1,
        l: 0
    }
    
    // This function accepts one argument, the result, which should be a string
    // Acceptable values are 'w', 'l', or 'd'
    const getPointsFromResult = function getPointsFromResult(result) {
        return RESULT_VALUES[result];
    }
    
    // Create getTotalPoints function which accepts a string of results
    // including wins, draws, and losses i.e. 'wwdlw'
    // Returns total number of points won
    
    const getTotalPoints = function(results){

        let totalScore = 0;
        Array.from(results).forEach( element => {
            if (element == 'w'){
                totalScore += RESULT_VALUES[element];
            }else if(element == 'd'){
                totalScore += RESULT_VALUES[element]; 
            }            
        });

        return totalScore;
    }
    

   
    // Check getTotalPoints
    console.log(getTotalPoints('wwdl')); // should equal 7
    
     
    // create orderTeams function that accepts as many team objects as desired, 
    // each argument is a team object in the format { name, results }
    // i.e. {name: 'Sounders', results: 'wwlwdd'}
    // Logs each entry to the console as "Team name: points"
    
    const orderTeams = function( ... teams){

        for ( team of teams){
            console.log(`${team.name}: ${getTotalPoints(team.results)}`);
        }
    }
     
    
    // Check orderTeams
    orderTeams(
        {name: 'Sounders', results: 'wwdl'},
        {name: 'Galaxy', results: 'wlld'}
    ); 
    /*
    // should log the following to the console:
    // Sounders: 7
    // Galaxy: 4
    */
 })();