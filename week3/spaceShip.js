/*
 * Antonio A Eggermont
 * Class 2 - Excercise 4 - SPACESHIP
 * 
 * Create a constructior function SpaceShip. It should set two 
 * properties: name and topSpeed. It should also have a method
 * accelerate that logs to the console `${name} moving to ${topSpeed}`.
 * 
 * We don't want anyone to access ane and topSpeed, make these private!
 * 
 * Well, maybve we should let people change the topSpeed - but not the shop name!
 * Keep both of these private, but add a method that changs the topSpeed.
 * 
 * Call the constructor with a couple ships, change the topSpeed, and call the 
 * accelerate method. 
 * 
 */

const SpaceShip = function(shipName, speed){

    const name = shipName;
    let topSpeed = speed;

    this.accelerate = function(){
        console.log(`${name} moving to ${topSpeed}`);
    };

    this.changeSpeed = function(speed){
        topSpeed = speed;
    };
}


const spaceShip1 = new SpaceShip("Air Force 1", 1000);
const spaceShip2 = new SpaceShip("Air Force 2", 2000);
const spaceShip3 = new SpaceShip("Air Force 3", 3000);
const spaceShip4 = new SpaceShip("Air Force 4", 4000);

spaceShip1.accelerate();
spaceShip1.changeSpeed(1200);
spaceShip1.accelerate();

spaceShip2.accelerate();
spaceShip2.changeSpeed(2500);
spaceShip2.accelerate();

spaceShip3.accelerate();
spaceShip3.changeSpeed(3900);
spaceShip3.accelerate();

spaceShip4.accelerate();
spaceShip4.changeSpeed(5200);
spaceShip4.accelerate();
