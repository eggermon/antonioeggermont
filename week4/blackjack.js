/** 
 * Antonio A Eggermont
 * Class 4 - Excercise 3
 * 
 * Blackjack Game
 */


/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
const CardPlayer = function CardPlayer (name) {
  this.name = name;  
  this.hand = [];
  
  /**
   * Selects a card at random from deck and adds to 
   * handarray. 
   */
  this.drawCard = function(){    
    let pickedCard = Math.floor(Math.random() * blackjackDeck.length);
    this.hand.push(blackjackDeck[pickedCard]);
  };
};

const statusTag = document.getElementById('status');
const winnerStatusTag = document.getElementById('winnerStatus');
const playerHandTag = document.getElementById('playerHand');
const dealerHandTag = document.getElementById('dealerHand');

/**
 * Calculates the score of a Blackjack hand.
 * This function will calculate the total number of points the hand is worth, 
 * and determine if the hand isSoft
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  this.total = 0;
  this.isSoft = false;
  this.numOfAces = 0;

  for ( handItem of hand){
    if ( handItem.displayVal == 'Ace' ){
      if (this.numOfAces > 0 ){
        this.total += 1;   
      } else if (this.total > 21){
        this.total += 1; 
      } else {
        this.total += handItem.val;
        this.isSoft = true;
      }
    }else{
      this.total += handItem.val;  
    }
  }

  let blackjackScore = {
    total : this.total,
    isSoft : this.isSoft,
  };

  return blackjackScore;
}


 /**
 * Determines whether the dealer should draw another card
 *  - If the dealer's hand is 16 points or less, the dealer must draw another card
 *  - If the dealer's hand is exactly 17 points, and the dealer has an Ace valued at 11, 
 *    the dealer must draw another card
 *  - Otherwise if the dealer's hand is 17 points or more, the dealer will end her turn
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  // CREATE FUNCTION HERE

  console.log(">>> dealer hand <<<");
  console.log(dealerHand);
  let blackjackScore = calcPoints(dealerHand);
  
  console.log(">>> dealer blackjackScore ");
  console.log(blackjackScore);

  if ((blackjackScore.isSoft == true) && ( blackjackScore.val == 17 )){
    return true;
  } else if (blackjackScore.val  < 16){
    return true;
  }else{
    return false;
  }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  // CREATE FUNCTION HERE
  const limit = 21;
  let message = '';

  if ( playerScore == dealerScore ){
    message = 'Both players tied the round';
  }else if ((playerScore < limit) && ( dealerScore < limit)){
    if (playerScore < dealerScore){
      message = 'Dealer wins';
    }else{
      message = 'Player wins';
    }
  }
  return (`${message} Players Score: ${playerScore} Dealer Score: ${dealerScore}`);
}


/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player, showHandTag) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);

  if ( player.name == 'Player' ){
    playerHandTag.innerHTML = `${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`;
  } else if ( player.name == 'Dealer' ){
    dealerHandTag.innerHTML = `${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`;
  }

}



/**
 * Runs Blackjack Game
 */
const startGame = function() {

  let player = new CardPlayer('Player');
  let dealer = new CardPlayer('Dealer');  

  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  showHand(player);
  showHand(dealer);

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }

  statusTag.innerHTML = 'Player stands at ' +  playerScore;

  // IMPLEMENT DEALER LOGIC BELOW
  let dealerScore = calcPoints(dealer.hand).total;
  showHand(dealer);
  while (dealerScore < 21 && confirm(getMessage(dealerScore, player.hand[0]))){
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }

  if (dealerScore > 21 ){
    return 'Dealer went over 21 - he loses!';
  }

  statusTag.innerHTML = 'Dealer stands at ' + dealerScore;
  winnerStatusTag.innerHTML = determineWinner(playerScore, dealerScore);
  return determineWinner(playerScore, dealerScore);
}


const blackjackDeck = getDeck();
console.log(startGame());