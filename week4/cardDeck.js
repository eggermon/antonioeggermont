/**
 * Antonio A Eggermont
 * Class 4 - Homework Excercise 2 - CREATE A DECK OF CARDS 
 *
 *  Returns an array of 52 Cards
 *  @returns {Array} deck - a deck of cards
 */
const getDeck = function() {
  let cards = [];

  const cardSuits = ['hearts','diamons','spades','clubs'];

  for (cardSuit of cardSuits){
    console.log(cardSuit);
    
    for(let j=2; j<=10; j++){
      cards.push({
        val: j,
        displayVal: j.toString(),
        suit: cardSuit,
      });
    }
    
    cards.push({
      val: 10,
      displayVal: 'Jack',
      suit: cardSuit,
    });

    cards.push({
      val: 10,
      displayVal: 'Queen',
      suit: cardSuit,
    });
    
    cards.push({
      val: 10,
      displayVal: 'King',
      suit: cardSuit,
    });

    cards.push({
      val: 11,
      displayVal: 'Ace',
      suit: cardSuit,
    });
  }

  return cards;
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
