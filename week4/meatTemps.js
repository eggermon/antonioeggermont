/** 
 * Antonio A Eggermont
 * Class 4 - Excercise 1
 * 
 * FOOD IS COOKED?
 */

const MEATVALUES = {
    beef : { 'rare' : 125 , 'medium' : 135 , 'well' : 155 },
    chicken : { 'rare' : 140  , 'medium' : 155 , 'well' : 180 }
};

/**
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = function(kind, internalTemp, doneness) {
  if ( internalTemp >  MEATVALUES[kind][doneness] ) {
     return true;
  } else {
     return false;
  }
}

// Test function
console.log(foodIsCooked('chicken', 90, 'rare')); // should be false
console.log(foodIsCooked('chicken', 190, 'well')); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true

