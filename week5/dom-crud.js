/**
 * Antonio A Eggermont
 * Class 5 - Class Exercise
 * Product CRUD
 */

// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>

let body = document.getElementsByTagName('body')[0];
console.log(body);

let aElement = document.createElement('a');
aElement.setAttribute('id', 'cta');
let text = document.createTextNode('Buy Now!');
aElement.appendChild(text);

body.children[0].appendChild(aElement);

// Access (read) the data-color attribute of the <img>,
// log to the console
let imgAttr = document.getElementsByTagName('img');
console.log(imgAttr[0].getAttribute('data-color'));

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"

let ul = document.getElementsByTagName('ul');
ul[0].children[2].setAttribute('class', 'highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

body.children[0].children[3].remove()