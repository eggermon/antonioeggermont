/**
 * Antonio A Eggermont
 * Class 5 - Class Exercise
 * Pluses and Minuses
 */
const minusE1 = document.getElementById('clickmeMinus');
const plusE1  = document.getElementById('clickmePlus');
const totalClicksE = document.getElementById('totalCount');
let totalClicks = 0;

minusE1.addEventListener('click', function(e){
    totalClicks--;
    console.log(`count is ${totalClicks}`);
    totalClicksE.innerHTML = `Total: ${totalClicks}`;
});


plusE1.addEventListener('click', function(e){
    totalClicks++;
    console.log(`count is ${totalClicks}`);
    totalClicksE.innerHTML = `Total: ${totalClicks}`;
});

