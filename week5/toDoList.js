/**
 * Antonio A Eggermont
 * Class 5 - Home Exercise
 * Todo List
 */




// If an li element is clicked, toggle the class "done" on the <li>
// TODO: I am not sure I understand well this question so I assume I just need to
//       add a "done" class to the clicked <li> element. 

const getLiElements = document.getElementsByTagName("li");
const handleLiElClicks = function(e){
    e.stopPropagation();
    this.classList.add('li');
    this.classList.add('done');
}

for ( var i = 0; i < getLiElements.length; i++){
    getLiElements[i].addEventListener('click', handleLiElClicks, false);
}

// If a delete link is clicked, delete the li element / remove from the DOM
const removeFromDom = function(e){
  e.stopPropagation();
  this.parentNode.parentNode.removeChild(this.parentNode);
}

const getTasksToDelete = document.getElementsByClassName('delete');

for ( var i = 0 ; i < getTasksToDelete.length; i++ ){
  getTasksToDelete[i].addEventListener('click', removeFromDom, false);
}


// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>

const toBeMoved = document.getElementsByClassName('move');
const laterList = document.getElementsByClassName('later-list');
const todayList = document.getElementsByClassName('today-list');

const handleTaskMove = function(e){
  e.stopPropagation();

  if ( (this.parentNode.childNodes[3].getAttribute('class') === 'move toLater' ) && 
       (this.parentNode.childNodes[3].getAttribute('class') != null)){

    let tempTask = this.parentNode;
    tempTask.childNodes[3].text = 'Move to Today';
    tempTask.childNodes[3].classList.remove('toLater');
    tempTask.childNodes[3].classList.add('toToday');
    laterList[0].appendChild(tempTask);

  } else if ((this.parentNode.childNodes[3].getAttribute('class') === 'move toToday') && 
             (this.parentNode.childNodes[3].getAttribute('class') != null)){

    let tempTask = this.parentNode;
    tempTask.childNodes[3].text = 'Move to Later';
    tempTask.childNodes[3].classList.remove('toToday');
    tempTask.childNodes[3].classList.add('toLater');
    todayList[0].appendChild(tempTask);
  } 
}


for ( var i = 0; i < toBeMoved.length; i++){
  toBeMoved[i].addEventListener('click', handleTaskMove, false);
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>

  const liEl = document.createElement('li');
  const spanEl = document.createElement('span');
  const aEl1 = document.createElement('a');
  const aEl2 = document.createElement('a');
  const textNode = document.createTextNode(text);
  const labelDelete = document.createTextNode('Delete');

  aEl1.classList.add('move');
  aEl2.appendChild(labelDelete);
  aEl2.classList.add('delete');
  aEl2.addEventListener('click', removeFromDom, false);
  liEl.addEventListener('click', handleLiElClicks, false);
  aEl1.addEventListener('click', handleTaskMove, false);
  spanEl.appendChild(textNode);

  liEl.appendChild(document.createTextNode(' '));
  liEl.appendChild(spanEl);
  liEl.appendChild(document.createTextNode(' '));

  if (this.parentNode.parentNode.getAttribute('class') == 'today'){
    aEl1.classList.add('toLater');
    
    const labelMoveToLater = document.createTextNode('Move to Later');
    aEl1.appendChild(labelMoveToLater);
    liEl.appendChild(aEl1);
    liEl.appendChild(document.createTextNode(' '));
    liEl.appendChild(aEl2);
    liEl.appendChild(document.createTextNode(' '));
    todayList[0].appendChild(liEl);


  }else if( this.parentNode.parentNode.getAttribute('class') == 'later'){
    aEl1.classList.add('toToday');
  
    const labelMoveToToday = document.createTextNode('Move to Today');
    aEl1.appendChild(labelMoveToToday);
    liEl.appendChild(aEl1);
    liEl.appendChild(document.createTextNode(' '));
    liEl.appendChild(aEl2);
    liEl.appendChild(document.createTextNode(' '));
    laterList[0].appendChild(liEl);
  }
}

// Add this as a listener to the two Add links
const getAddLinks = document.getElementsByClassName('add-item');
for ( let i = 0 ; i < getAddLinks.length; i++){
  getAddLinks[i].addEventListener('click', addListItem );
}
