
/**
 * Antonio Eggermont
 * Class 6 Exercise - Car Constructor
 */


const Car = function(model){
    this.model = model;
    this.currentSpeed = 0;
};

Car.toString.getCurrentSpeed = function(){
        return `Current Speed: ${this.currentSpeed}`;
};

Car.prototype.accelerate = function(){
        this.currentSpeed +=1;
};

Car.prototype.brake = function(){
        this.currentSpeed -=1;
};

Car.prototype.toString = function() {
    return `${this.model} going at speed of ${this.currentSpeed}`; 
};






const newCar = new Car('Taurus');
newCar.accelerate();
newCar.accelerate();
newCar.accelerate();
newCar.brake();

console.log(newCar.toString());