/**
 *  Antonio Eggermont - Class Exercise Crude Returns
 * 
 * Given the HTML page from the class 6 repo (jQueryDomCrud.html) 
 * implement the following
 */ 

$(document).ready(function(){
    
    // Access (read) the data-color attribute of the <img>,
    // log to the console

    const $img = jQuery('img');
    console.log($img.attr('data-color'));

    // Update the third <li> item ("Turbocharged"), 
    // set the class name to "highlight"

    let li3 = $('li')[2];
    let $li3 = $(li3);
    $li3.addClass('highlight');
});







// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
