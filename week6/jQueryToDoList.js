/**
 * Antonio Eggermont - Class 6 - Homework Assignment JQuery ToDo List
 * Creates a new instance of Pizza
 */

$(document).ready(function(){
    console.log("page loaded ... ");
    // If an li element is clicked, toggle the class "done" on the <li>

    let liElements = $('li');
    $('li').on('click', function(e) {
        $(this).toggleClass('done');
    });

    // If a delete link is clicked, delete the li element / remove from the DOM
    const deleteItem = function (e){
        e.preventDefault();
        // TODO: Complete fade out effect on deleted item
        //$(this).parent().fadeOut( "slow", function(){
        //    e.stopPropagation();
        //    $(this).parent().remove();
        //});
        $(this).parent().remove();
    }

    $('a.delete').on('click', deleteItem);

    
    // If a "Move to..."" link is clicked, it should move the item to the correct
    // list.  Should also update the working (i.e. from Move to Later to Move to Today)
    // and should update the class for that link.
    // Should *NOT* change the done class on the <li>
    const moveToToday = function(e){
        e.stopPropagation();
        let liEl = $(this);
        liEl.text('Move to Later');
        liEl.removeClass('toToday');
        liEl.addClass('toLater');  
        liEl.on('click', moveToLater);
        liEl.parent().appendTo($('ul.today-list'));
    }

    const moveToLater = function(e){
        e.stopPropagation();
        let liEl = $(this);
        liEl.text('Move to Today');
        liEl.removeClass('toLater');
        liEl.addClass('toToday');
        liEl.on('click', moveToToday);
        liEl.parent().appendTo($('ul.later-list'));
    }

    $('a.move.toLater').on('click', moveToLater);
    $('a.move.toToday').on('click', moveToToday);
   

    // If an 'Add' link is clicked, adds the item as a new list item in correct list
    // addListItem function has been started to help you get going!  
    // Make sure to add an event listener to your new <li> (if needed)
    const addListItem = function(e) {
        e.preventDefault();
        e.stopPropagation();
        const text = $(this).parent().find('input').val();
        let $newLiEl = $('<li>');
        let $newSpanEl = $('<span>');
        let $newAEl1 = $('<a>');
        let $newAEl2 = $('<a>');

        $newAEl2.text('Delete');
        $newAEl2.addClass('delete');
        $newAEl2.on('click', deleteItem);
        $newAEl2.appendTo($newLiEl);

        $newLiEl.text(text);

        $newSpanEl.appendTo($newLiEl);
        $newLiEl.appendTo($newLiEl);

        if ($(this).parent().parent().hasClass('today')){
            $newAEl1.text('Move to Later');
            $newAEl1.addClass('move toLater');
            $newAEl1.appendTo($newLiEl);
            $newAEl2.appendTo($newLiEl);
            $newAEl1.on('click', moveToLater);
            $newLiEl.appendTo($('ul.today-list'));

        }else if ($(this).parent().parent().hasClass('later')){
            $newAEl1.text('Move to Today');
            $newAEl1.addClass('move toToday');
            $newAEl1.appendTo($newLiEl);
            $newAEl2.appendTo($newLiEl);
            $newAEl1.on('click', moveToToday);
            $newLiEl.appendTo($('ul.later-list'));
        }
    }

    $('a.add-item').on('click', addListItem);

    // Add this as a listener to the two Add links
});