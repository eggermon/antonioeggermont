/**
 * Antonio Eggermont - Class 6 - Homework Assignment 2
 * Creates a new instance of Pizza
 * @param {String} kind 
 */

const Pizza = function(kind){
    this.kind = kind;
    this.slices = 8;
};

Pizza.prototype.eatSlice = function () {
    this.slices -= 1;
};

Pizza.prototype.toString = function() {
    return `${this.kind} has ${this.slices} slices left`; 
}


Pizza.prototype.getSlicesLeft = function () {
    return this.slices;
};

pizza1 = new Pizza('Peperoni');
pizza1.eatSlice();
console.log(pizza1.toString());


pizza2 = new Pizza('Vegan');
pizza2.eatSlice();
pizza2.eatSlice();
pizza2.eatSlice();
pizza2.eatSlice();
console.log(pizza2.toString());


