/**
 * Antonio Eggermont
 * Class 6 Exercise - Where's the pointer?
 */

const allTdElements = document.getElementsByTagName('td');
for ( let i = 0; i < allTdElements.length; i++){
    allTdElements[i].addEventListener('click', function(e){
        this.innerHTML = `${e.clientX} , ${e.clientY}`;
    });
}
console.log(allTdElements);