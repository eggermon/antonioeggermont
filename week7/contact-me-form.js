/**
 * Antonio Eggermont - Class 7 Contact Me Form Assignment 
 */

const validateItem = function(inputEl, submitEvent){
    const errorEl = inputEl.parentElement.querySelector('.error');
    const firstName = document.getElementsByTagName('input')[0];
    let isValid = true;
    
    if (inputEl.getAttribute('name') === 'message'){
        if (inputEl.value  === '' ||  inputEl.value.length < 10){
            const labelEl = inputEl.parentElement.querySelector('label');
            errorEl.innerHTML = `${labelEl.innerText} is Required with 10 or more characters`;
            inputEl.parentElement.classList.add('invalid');
            isValid = false;
            
        }else{
            errorEl.innerHTML ='';
            inputEl.parentElement.classList.remove('invalid');
            submitEvent.preventDefault(); 
        }
    } else if ( inputEl.getAttribute('type') === 'text' ){
        if (inputEl.value  === '' ||  inputEl.value.length < 3){
            const labelEl = inputEl.parentElement.querySelector('label');

            if (!inputEl.value){
                errorEl.innerHTML = `${labelEl.innerText} is Required`;
            }else{
                errorEl.innerHTML = `${labelEl.innerText} is Not Valid`;
            }
            
            isValid = false;
            inputEl.parentElement.classList.add('invalid');
            
        }else{
            errorEl.innerHTML ='';
            inputEl.parentElement.classList.remove('invalid');
            submitEvent.preventDefault();
        }

    } else if(inputEl.getAttribute('type') === 'email' ){
        const labelEl = inputEl.parentElement.querySelector('label');

        let pattern = /\w+@\w+\.\w+/;
        if (pattern.exec(inputEl.value)){
            inputEl.parentElement.classList.remove('invalid');
            errorEl.innerHTML ='';
            
        }else{
            errorEl.innerHTML = `Valid ${labelEl.innerText} is Required`;
            inputEl.parentElement.classList.add('invalid');
            isValid = false;
            submitEvent.preventDefault();
            
        }
    }
    return  isValid;    
}

const inputElements = document.getElementsByClassName('validate-input');
const formSelect = document.querySelector('select');

formSelect.addEventListener('change', function(e){
    if ( this.value ==='business'){  
        if (this.parentElement.classList.contains('businessreason') === false){

            let divFormGroupEl = document.createElement('div');
            divFormGroupEl.classList.add('form-group');
            divFormGroupEl.setAttribute('name', 'business-reason');
            
            let inputElJobTitle = document.createElement('input');
            inputElJobTitle.setAttribute('type', 'text');
            inputElJobTitle.setAttribute('name', 'job-title');

            let labelElJobTitle = document.createElement('label');
            labelElJobTitle.innerHTML = 'Job title: ';
            this.parentElement.appendChild(labelElJobTitle);
            this.parentElement.appendChild(inputElJobTitle);

            let inputElCompWebSite = document.createElement('input');
            inputElCompWebSite.setAttribute('type', 'text');
            inputElCompWebSite.setAttribute('name', 'company-site');

            let labelElCompWebSite = document.createElement('label');
            labelElCompWebSite.innerHTML = 'Company Web Site:';
            
            divFormGroupEl.appendChild(labelElCompWebSite);
            divFormGroupEl.appendChild(inputElCompWebSite);
            this.parentElement.appendChild(divFormGroupEl);
            this.parentElement.classList.add('businessreason');
        }

    } else if ( this.value ==='technical' ){
        if (this.parentElement.classList.contains('techreason') === false){
            let divFormGroupEl = document.createElement('div');
            divFormGroupEl.classList.add('form-group');
            divFormGroupEl.setAttribute('name', 'tech-reason');

            let inputElCodLang = document.createElement('input');
            inputElCodLang.setAttribute('type', 'text');
            inputElCodLang.setAttribute('name', 'code-languaje');

            let labelElCodLangTitle = document.createElement('label');
            labelElCodLangTitle.innerHTML = 'Coding language: ';
            
            divFormGroupEl.appendChild(labelElCodLangTitle);
            divFormGroupEl.appendChild(inputElCodLang);
            this.parentElement.appendChild(divFormGroupEl);
            this.parentElement.classList.add('techreason');
        }
    }
});



const formEl = document.getElementById('form-to-submitt')
    .addEventListener('submit', function(e) {
        let isValid = true;

        for ( let i = 0; i < inputElements.length; i++){
            isValid = validateItem(inputElements[i], e);
        }

        if (isValid){
            localStorage.setItem('contact_type', 'business');
        }
     
    });