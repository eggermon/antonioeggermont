/**
 * Antonio Eggermont - Class 7 Connect with US custom Validation 
 */

const validateItem = function(inputEl, submitEvent){    
    if (inputEl.getAttribute('type') === 'text' ){
        if (inputEl.value.length < 3){
            let text = document.createTextNode("value must be at least 3 characters");
            inputEl.parentElement.classList.add('invalid')
            inputEl.parentElement.childNodes[5].appendChild(text);
        }else{
            let text = document.createTextNode("");
            inputEl.parentElement.classList.remove('invalid');
            inputEl.parentElement.childNodes[5].innerHTML = "";
            submitEvent.preventDefault(); 
        }
    }else if(inputEl.getAttribute('type') === 'email' ){
        let pattern = /\w+@\w+\.\w+/;
        
        if (pattern.exec(inputEl.value)){
            inputEl.parentElement.classList.remove('invalid');
            inputEl.parentElement.childNodes[5].innerHTML = "";
        }else{
            let text = document.createTextNode("email is not valid");
            inputEl.parentElement.classList.add('invalid');
            inputEl.parentElement.childNodes[5].appendChild(text);
            submitEvent.preventDefault(); 
        }
    }
}

const inputElements = document.getElementsByClassName('validate-input');

const formEl = document.getElementById('form-to-submitt')
    .addEventListener('submit', function(e){
        for ( let i = 0; i < inputElements.length; i++){
            validateItem(inputElements[i], e);
        }
    });