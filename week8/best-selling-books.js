/**
 * Antonio Eggermont - Class Exercise - Fetch Bestselling Books
 */

const API_KEY = "<YOU API KEY>";
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists';
const MAX_BOOKS = 5;
const ulElem = document.getElementById('results');

const getData = function(year, month, day, submitEvent){
    console.log(year);
    console.log(month);
    console.log(day);

    let url = `${BASE_URL}/${year}-${month}-${day}/hardcover-fiction.json?api-key=${API_KEY}`;
    console.log(url);

    fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(responseJson) {
        console.log(responseJson);
        window.responseJson = responseJson;

        if ( responseJson.results.books.length > 0 ){

            for ( let i = 0; i < MAX_BOOKS; i++){
                let liEle =  document.createElement('li');
                let brEle1 = document.createElement('br');
                let brEle2 = document.createElement('br');
                let brEle3 = document.createElement('br');
                let authorText = document.createTextNode(responseJson.results.books[i].author);
                let titleText = document.createTextNode(responseJson.results.books[i].title);
                let descriptionText = document.createTextNode(responseJson.results.books[i].description);
                let imgElem = document.createElement("img");
                imgElem.setAttribute('src', responseJson.results.books[i].book_image);
                imgElem.setAttribute('height', '50px');
                imgElem.setAttribute('width', '50px');

                liEle.setAttribute('class', 'list-group-item');
                liEle.appendChild(titleText);
                liEle.appendChild(brEle1);
                liEle.appendChild(authorText);
                liEle.appendChild(brEle2);
                liEle.appendChild(descriptionText);
                liEle.appendChild(brEle3);
                liEle.appendChild(imgElem);
                ulElem.appendChild(liEle);
            }    
        }
        
    });
};




const formEl = document.getElementById('form-to-submitt')
    .addEventListener('submit', function(e) {
        const year = document.getElementById('year').value;
        const month = document.getElementById('month').value;
        const day = document.getElementById('day').value;
        getData(year, month, day, e );

        e.preventDefault();
    });