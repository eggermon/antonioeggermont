/**
 * Antonio Eggermont - Class Exercise - fetch exercise 
 */

const API_KEY = "<YOU API KEY>";
const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
const url = `${BASE_URL}?q=javascript&api-key=${API_KEY}`;

console.log(">>> loaded.... <<<");

fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(responseJson) {
        console.log(responseJson);
        window.responseJson = responseJson;
        let article = responseJson.response.docs[0];
        

        const mainHeadline = article.headline.main;
        document.getElementById('article-title').innerHTML = mainHeadline;

        if (article.multimedia.length > 0) {
            const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
            document.getElementById('article-img').src = imgUrl;
        }
    
        document.getElementById('article-snippet').innerHTML = article.snippet;
        document.getElementById('article-link').innerHTML = article.web_url;
        document.getElementById('pub-author').innerHTML = article.byline.original;
        document.getElementById('pub-main').innerHTML = article.headline.main;


        console.log(">>> article <<<");
        console.log(article.headline.main);
        console.log(article.snippet);
        console.log(article.web_url);
        console.log(article.pub_date);
        console.log(article.byline.original);
        console.log(">>>>>>>>>>>>>>>");
    })