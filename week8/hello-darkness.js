/**
 * Antonio Eggermont - Class Exercise - setInterval: Hello Darkness 
 */

const divMaintenanceEl = document.getElementById('body-document');
let colorValue = 255;


const darken = function(){
    colorValue--;
    console.log(`rgb(${colorValue}, ${colorValue}, ${colorValue})`);

    document.body.style.backgroundColor = `rgb(${colorValue}, ${colorValue}, ${colorValue})`;
    
    if ( colorValue === 0 ){
        clearInterval(interval);
    }
}

let interval = setInterval(darken, 500);
