/**
 * Antonio Eggermont - Class Exercise - PROMISES
 */

let myPromise = new Promise(function(resolve, reject) {
    setTimeout(function(){
        let randomNum = Math.random();
        console.log(randomNum);

        if (randomNum > 0.5){
            resolve();
        }else{
            reject();
        }
    }, 3000);
});

let mySecondPromise = myPromise.then(function() {
    console.log('success!!');
});

let myThirdPromise = mySecondPromise.catch(function() {
    console.log('fail!!');
});

myThirdPromise.then(function() {
    console.log('complete!!');
});