/**
 * Antonio Eggermont - Class Exercise - setTimeout: Remove the Notice
 */

const divMaintenanceEl = document.getElementById('notice-maintenance');

const removeNotice = function(){
    divMaintenanceEl.style.display = 'none';
    divMaintenanceEl.classList.add('hidden');
 }

 const myVar = setInterval(removeNotice, 5000);